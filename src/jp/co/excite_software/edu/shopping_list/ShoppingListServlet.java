package jp.co.excite_software.edu.shopping_list;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShoppingListServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /* TODO:
         * クライアント（ブラウザ）からの GET メソッドによるリクエストを処理すること。
         * 当該サーブレットに対する GET メソッドによる呼び出しは、
         * 未購入の商品を一覧表示する画面（JSP）にリダイレクトすること。
         */
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /* TODO:
         * クライアント（ブラウザ）からの POST メソッドによるリクエストを処理すること。
         * 当該サーブレットに対する POST メソッドによる呼び出しは、以下の３種類。
         * ・商品の新規追加
         * ・商品内容の更新
         * ・商品の削除
         * 
         * 上記の処理を行った後、
         * 未購入の商品を一覧表示する画面（JSP）にリダイレクトすること。
         * 
         * ※RESTは意識していないのでその辺のツッコミはなしでお願いします。
         */
    }

    private void registerGoods(String name, int number, String memo) {
        /* TODO:
         * 引数で指定された「商品名」「個数」「メモ」をもとに、新たな商品を登録すること。
         */
    }

    private void updateGoods(String uuid, String name, int number, String memo) {
        /* TODO:
         * 引数で指定された「UUID」に該当するデータの「商品名」「個数」「メモ」を更新すること。
         */
    }

    private void deleteGoods(String uuid) {
        /* TODO:
         * 引数で指定された「UUID」に該当するデータを削除すること。
         */
    }

    private void purchaseGoods(String uuid) {
        /* TODO:
         * 引数で指定された「UUID」に該当するデータを“購入済み”として更新すること。
         */
    }

}
