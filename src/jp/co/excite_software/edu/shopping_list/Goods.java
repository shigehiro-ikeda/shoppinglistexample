package jp.co.excite_software.edu.shopping_list;

public class Goods {

    /**  */
    private String uuid;
    /**  */
    private String name;
    /**  */
    private Integer number;
    /**  */
    private String memo;
    /**  */
    private String registeredDatetime;
    /**  */
    private String purchasedDatetime;
    /**  */
    private String updatedDatetime;

    /**
     * 
     * @param uuid
     * @param name
     * @param number
     * @param memo
     * @param registeredDatetime
     * @param purchasedDatetime
     * @param updatedDatetime
     */
    public Goods(String uuid,
                 String name,
                 Integer number,
                 String memo,
                 String registeredDatetime,
                 String purchasedDatetime,
                 String updatedDatetime) {
        this.uuid = uuid;
        this.name = name;
        this.number = number;
        this.memo = memo;
        this.registeredDatetime = registeredDatetime;
        this.purchasedDatetime = purchasedDatetime;
        this.updatedDatetime = updatedDatetime;
    }
    /**
     * 
     * @param name
     * @param number
     * @param memo
     */
    public Goods(String name,
                 Integer number,
                 String memo) {
        this.uuid = java.util.UUID.randomUUID().toString();
        this.name = name;
        this.number = number;
        this.memo = memo;
    }

    /**
     * 
     * @return
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @return
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 
     * @return
     */
    public String getMemo() {
        return memo;
    }

    /**
     * 
     * @return
     */
    public String getRegisteredDatetime() {
        return registeredDatetime;
    }

    /**
     * 
     * @return
     */
    public String getPurchasedDatetime() {
        return purchasedDatetime;
    }

    /**
     * 
     * @return
     */
    public String getUpdatedDatetime() {
        return updatedDatetime;
    }

    @Override
    public String toString() {
        return String.format("%s:[%s (%d) %s] %s: %s: %s", uuid, name, number, memo, registeredDatetime, purchasedDatetime, updatedDatetime);
    }
}
