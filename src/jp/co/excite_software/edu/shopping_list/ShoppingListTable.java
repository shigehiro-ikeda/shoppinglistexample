package jp.co.excite_software.edu.shopping_list;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShoppingListTable {

    /**  */
    public static final String UUID = "UUID";
    /**  */
    public static final String NAME = "NAME";
    /**  */
    public static final String NUMBER = "NUMBER";
    /**  */
    public static final String MEMO = "MEMO";
    /**  */
    public static final String REGISTERED_DATETIME = "REGISTERED_DATETIME";
    /**  */
    public static final String PURCHASED_DATETIME = "PURCHASED_DATETIME";
    /**  */
    public static final String UPDATED_DATETIME = "UPDATED_DATETIME";

    /** 接続先DBのURL */
    private static String dbUrl = "jdbc:mysql://localhost:3306/PRACTICE";
    /** DBの接続ユーザー */
    private static String dbUser = "practice";
    /** DBの接続パスワード */
    private static String dbPassword = "practice";

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        // 以下はこのクラスの確認用です
        try {
//            Class.forName("com.mysql.jdbc.Driver"); // JDK 1.6 以降ではこの記述は不要

            ShoppingListTable table = new ShoppingListTable();
            List<Goods> all = table.getAll();
            for (Goods goods : all) {
                System.out.format("%s: %s (%d) %s", goods.getUuid(), goods.getName(), goods.getNumber(), goods.getMemo());
                System.out.println();
            }

            System.out.println();
            for (int i = 0; i < 10; i++) {
                Goods g = new Goods("どん兵衛", 0, "新規作成テスト");
                // uuid がユニークに生成されていることを確認
                System.out.println(g.toString());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 引数で渡された ResultSet について、
     * SHOPPING_LIST テーブルに対する select の結果とみなして
     * List<Goods> 型に入れ直して返します。
     * 
     * @param rs SHOPPING_LIST テーブルに対する select の結果セット
     * @return select の結果を List<Goods> に入れ直した結果
     * @throws SQLException
     */
    private List<Goods> ToList(ResultSet rs) throws SQLException {

        ArrayList<Goods> result = new ArrayList<Goods>();

        while(rs.next()) {
            String uuid = rs.getString(UUID);
            String name = rs.getString(NAME);
            int number = rs.getInt(NUMBER);
            String memo = rs.getString(MEMO);
            String registeredDatetime = rs.getString(REGISTERED_DATETIME);
            String purchasedDatetime = rs.getString(PURCHASED_DATETIME);
            String updatedDatetime = rs.getString(UPDATED_DATETIME);

            Goods goods = new Goods(uuid, name, number, memo, registeredDatetime, purchasedDatetime, updatedDatetime);

            result.add(goods);
        }

        return result;
    }

    /**
     * SHOPPING_LIST テーブルのすべてのレコードを返します。
     * @return SHOPPING_LIST テーブルのすべてのレコード
     * @throws SQLException
     */
    public List<Goods> getAll() throws SQLException {

        String sql =
                "select *"
                + " from SHOPPING_LIST"
                + " order by UPDATED_DATETIME, REGISTERED_DATETIME";

        try (Connection conn = DriverManager.getConnection(dbUrl, dbUser, dbPassword)) {
            conn.setReadOnly(true);
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            return ToList(rs);
        }
    }

    /**
     * 
     * @return
     */
    public List<Goods> getAllNotYetParchesed() {
        /* TODO:
         * 登録済みの商品のうち未購入の商品をすべて取得し、
         * List<Goods> に代入して返却すること。
         */
        return null;
    }

    /**
     * 
     * @param uuid
     * @return
     */
    public Goods get(String uuid) {
        /* TODO:
         * 指定された UUID に一致する商品のレコードを取得し、
         * Goods オブジェクトにして返却すること。
         */
        return null;
    }

    /**
     * 
     * @param goods
     * @return
     */
    public Goods add(Goods goods) {
        /* TODO:
         * 引数で渡された Goods を SHOPPING_LIST テーブルへ挿入すること。
         */
        return null;
    }

    /**
     * 
     * @param goods
     */
    public void update(Goods goods) {
        /* TODO:
         * 引数で渡された Goods を SHOPPING_LIST テーブルに対して更新すること。
         * 
         */
    }

    /**
     * 
     * @param uuid
     */
    public void delete(String uuid) {
        /* TODO:
         * 指定された UUID に一致する商品のレコードを削除すること。
         * 
         */
    }

}
