
drop database PRACTICE;

create database PRACTICE;
use PRACTICE;

create user 'practice'@'localhost' identified by 'practice';

grant all privileges on PRACTICE.* to 'practice'@'localhost';

flush privileges;

drop table SHOPPING_LIST;

/**********************************/
/* テーブル名: 買い物リスト */
/**********************************/
CREATE TABLE SHOPPING_LIST(
		UUID                          		CHAR(37)		 NOT NULL COMMENT 'UUID',
		NAME                          		VARCHAR(30)		 NOT NULL COMMENT '商品名',
		NUMBER                        		SMALLINT(3)		 NULL  COMMENT '個数',
		MEMO                          		VARCHAR(200)	 NULL  COMMENT 'メモ',
		REGISTERED_DATETIME           		DATETIME		 NOT NULL COMMENT '登録日時',
		PURCHASED_DATETIME            		DATETIME		 NULL  COMMENT '購入日時',
		UPDATED_DATETIME              		DATETIME		 NULL  COMMENT '更新日時'
) COMMENT='買い物リスト';

ALTER TABLE SHOPPING_LIST ADD CONSTRAINT IDX_SHOPPING_LIST_PK PRIMARY KEY (UUID);

